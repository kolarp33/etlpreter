import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ETLPreter",
    version="0.0.1",
    author="Peter Kolárovec",
    author_email="kolarp33@fit.cvut.cz",
    description="Generator of dynamic SQL",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.fit.cvut.cz/kolarp33/etlpreter",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)