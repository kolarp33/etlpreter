class BadApiCallError(BaseException):
    """Raised in etlpreter if there is unknown API call"""

    def __init__(self, call, message):
        self.call = call
        self.message = message
