class MissingValueError(BaseException):
    """Raised in API when called attribute in function is missing"""

    def __init__(self, attribute, message):
        self.attribute = attribute
        self.message = message
