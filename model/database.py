import sys
import os
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData
import configparser
import logging
from sqlalchemy.orm import relationship, joinedload, subqueryload, Session, sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base, declared_attr
import time
import logging
import model.tables
from urllib.parse import quote_plus

Base = declarative_base()
cfg = configparser.ConfigParser()
path = os.path.realpath(os.path.dirname(__file__) + '/../config/config.ini')


class Database():

    def connect(self):
        try:
            # logging.basicConfig(filename='database.log',level=logging.DEBUG)
            cfg.read(path)
            cfg.sections()

            if cfg.get('serverSRC', 'type') == 'mysql':
                connection_string = 'mysql+pymysql://'+cfg.get('serverSRC', 'user')+':'+cfg.get('serverSRC', 'password') + \
                    '@'+cfg.get('serverSRC', 'host')+':'+cfg.get(
                    'serverSRC', 'port')+'/'+cfg.get('serverSRC', 'dbname')

            # print(connection_string)
            engine = create_engine(connection_string)
            Base.metadata.create_all(engine)
            Session = sessionmaker(bind=engine)
            session = Session()
            return session
        except Exception as e:
            print(str(e))
        finally:
            session.close()
