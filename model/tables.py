from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from model.database import Base


class etl_knowledge_module(Base):
    """Head for KM steps in one Knowledge module"""
    __tablename__ = 'ETL_KNOWLEDGE_MODULE'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    version = Column(Integer)


class etl_km_steps(Base):
    """SQL steps of the knowledge module"""
    __tablename__ = 'ETL_KM_STEPS'

    id = Column(Integer, primary_key=True, autoincrement=True)
    step = Column(String(250))
    comment = Column(String(100))
    km_id = Column(Integer, ForeignKey('ETL_KNOWLEDGE_MODULE.id'))
    km = relationship('etl_knowledge_module', backref='ETL_KM_STEPS')


class etl_map_tables(Base):
    """Src/Trg tables"""
    __tablename__ = 'ETL_MAP_TABLES'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250))
    definition = Column(String(4000))
    columns = Column(String(4000))
    compare_key = Column(String(4000))
    scd_columns = Column(String(4000))


class etl_map(Base):
    """Control table"""
    __tablename__ = 'ETL_MAP'

    id = Column(Integer, primary_key=True, autoincrement=True)
    src_id = Column(Integer, ForeignKey('ETL_MAP_TABLES.id'))
    trg_id = Column(Integer, ForeignKey('ETL_MAP_TABLES.id'))

    km_id = Column(Integer, ForeignKey('ETL_KNOWLEDGE_MODULE.id'))
    src = relationship('etl_map_tables', backref='ETL_MAP1',
                       foreign_keys=[src_id])
    trg = relationship('etl_map_tables', backref='ETL_MAP2',
                       foreign_keys=[trg_id])
    km = relationship('etl_knowledge_module',
                      backref=backref('ETL_MAP', uselist=False))


class etl_hist(Base):
    """Interpreted knowledge modules"""
    __tablename__ = 'ETL_HIST'

    id = Column(Integer, primary_key=True, autoincrement=True)
    etl_map_id = Column('ETL_MAP_ID', Integer, ForeignKey('ETL_MAP.id'))
    etl_km_id = Column('ETL_KM_ID', Integer,
                       ForeignKey('ETL_KNOWLEDGE_MODULE.id'))
    etl_km_steps_id = Column('ETL_KM_STEP_ID', Integer,
                             ForeignKey('ETL_KM_STEPS.id'))
    etl_km_step = Column('ETL_KM_STEP', String(4000))
    loaddatetime = Column('LOADDATETIME', DateTime)


class etl_km_options(Base):
    """Defined options for KM"""
    __tablename__ = 'ETL_KM_OPTIONS'

    id = Column(Integer, primary_key=True, autoincrement=True)
    value = Column(String(250))
    opt_id = Column(Integer, ForeignKey('ETL_OPTIONS.id'))
    km_id = Column(Integer, ForeignKey('ETL_KNOWLEDGE_MODULE.id'))
    km = relationship('etl_knowledge_module', backref=backref(
        'ETL_KM_OPTIONS', uselist=False))
    opt = relationship('etl_options', backref=backref(
        'ETL_KM_OPTIONS', uselist=False))


class etl_options(Base):
    """Proposed options"""
    __tablename__ = 'ETL_OPTIONS'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250))
    type = Column(String(250))
    default_value = Column(String(250))
    description = Column(String(250))
