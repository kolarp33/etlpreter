INSERT into ETL_KNOWLEDGE_MODULE(NAME,VERSION) values ('DV_HUB',1);
INSERT into ETL_KNOWLEDGE_MODULE(NAME,VERSION) values ('DV_SAT',1);
INSERT into ETL_KNOWLEDGE_MODULE(NAME,VERSION) values ('DV_LINK',1);
INSERT into ETL_KNOWLEDGE_MODULE(NAME,VERSION) values ('DIM_SCD2',1);

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (1,'IF (OBJECT_ID(\'\'tempdb..<%get.tmp.trg.name%>\'\') IS NOT NULL) 
    DROP TABLE <%get.tmp.trg.name%>;','drop tmp, drop src');
  
INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (1,'select * 
INTO <%get.tmp.trg.name%>
from (<%get.src.definition%>) src','create tmp');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (2,'IF (OBJECT_ID(\'\'tempdb..<%get.tmp.trg.name%>\'\') IS NOT NULL) 
    DROP TABLE <%get.tmp.trg.name%>;','drop tmp, drop src');
  
INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (2,'select * 
INTO <%get.tmp.trg.name%>
from (<%get.src.definition%>) src','create tmp');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (3,'IF (OBJECT_ID(\'\'tempdb..<%get.tmp.trg.name%>\'\') IS NOT NULL) 
    DROP TABLE <%get.tmp.trg.name%>;','drop tmp, drop src');
  
INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (3,'select * 
INTO <%get.tmp.trg.name%>
from (<%get.src.definition%>) src','create tmp');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (4,'IF (OBJECT_ID(\'\'tempdb..<%get.tmp.trg.name%>\'\') IS NOT NULL) 
    DROP TABLE <%get.tmp.trg.name%>;','drop tmp, drop src');
  
INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (4,'select * 
INTO <%get.tmp.trg.name%>
from (<%get.src.definition%>) src','create tmp');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (1,'BEGIN TRY
	BEGIN TRANSACTION

		INSERT INTO <%get.trg.definition%>
		(<%get.trg.columns%>)
		SELECT <%get.trg.columns(tmp)%>
		FROM <%get.tmp.trg.name%> AS tmp
		LEFT JOIN 	(SELECT b.<%get.trg.compare_key%>, b.SourceSystem
							FROM <%get.tmp.trg.name%> b
							JOIN <%get.trg.definition%>
								on b.<%get.trg.compare_key%> = <%get.trg.definition%>.<%get.trg.compare_key%>
								and b.SourceSystem = <%get.trg.definition%>.SourceSystem
					) b
		ON tmp.<%get.trg.compare_key%> = b.<%get.trg.compare_key%> AND tmp.SourceSystem = b.SourceSystem
		WHERE b.<%get.trg.compare_key%> IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (<%get.etl.option(debug)%> = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH','dv_hub');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (2,'BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO <%get.trg.definition%>
				(<%get.trg.columns%>)
				SELECT  <%get.trg.columns(tmp)%>
				FROM <%get.tmp.trg.name%> AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by <%get.trg.compare_key%> 
							  								order by b.LoadDatetime desc)rn 
							from <%get.trg.definition%> as b )s
      						where rn = 1) AS b 
				ON tmp.<%get.trg.compare_key%> = b.<%get.trg.compare_key%> 
				AND tmp.SourceSystem = b.SourceSystem
   		WHERE b.<%get.trg.compare_key%> IS NULL
   		OR tmp.HashDiff <> b.HashDiff;

IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (<%get.etl.option(debug)%> = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH','dv_sat');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (3,'BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO <%get.trg.definition%>
				(<%get.trg.columns%>)
				SELECT  <%get.trg.columns(tmp)%>
				FROM <%get.tmp.trg.name%> AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by <%get.trg.compare_key%> 
							  								order by b.LoadDatetime desc)rn 
							from <%get.trg.definition%> as b )s
      						where rn = 1) AS b  
				ON tmp.<%get.trg.compare_key%> = b.<%get.trg.compare_key%> 
				AND tmp.SourceSystem = b.SourceSystem
				WHERE b.<%get.trg.compare_key%> IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (<%get.etl.option(debug)%> = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH','dv_link');

INSERT into ETL_KM_STEPS(KM_ID,STEP,COMMENT) values (4,'INSERT INTO <%get.trg.definition%>
SELECT
    <%get.src.columns%>
    ,getdate() as ValidFrom
    ,\'\'30000101\'\' as ValidTo
    ,1 as CurrentFlag
FROM
(
    MERGE <%get.trg.definition%> AS Target
    USING <%get.tmp.trg.name%> AS Source
    ON Source.<%get.src.compare_key%> = Target.<%get.trg.compare_key%> AND Target.CurrentFlag = 1

    WHEN MATCHED AND
    (
        <%get.scd_columns(Source,Target)%>
    )
    THEN UPDATE 
    SET
        Target.CurrentFlag = 0,
        Target.[ValidTo] = getdate()-1
    WHEN NOT MATCHED
    THEN INSERT
    (
        <%get.trg.columns%>
        ,ValidFrom
        ,ValidTo
        ,CurrentFlag
    )
    VALUES
    (
        <%get.src.columns(Source)%>
        ,getdate()
        ,\'\'30000101\'\'
        ,1
    )
    OUTPUT $action AS Action, Source.* 
) as MergeOutput
WHERE MergeOutput.Action = \'\'UPDATE\'\';','update and merge');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('src_test_hub_customer',
       'SELECT  HASHBYTES(\'\'SHA1\'\', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        CID as Customer_ID
FROM stage.dbo.Cust',
       'Customer_HK,LoadDatetime,SourceSystem,Customer_ID',
       '',
       'Customer_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('trg_test_hub_customer',
       'core.dbo.H_Customer',
       'Customer_HK,LoadDatetime,SourceSystem,Customer_ID',
       '',
       'Customer_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('src_test_sat_customer',
       'SELECT  HASHBYTES(\'\'SHA1\'\', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        HASHBYTES(\'\'SHA1\'\', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,\'\' \'\',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust',
       'Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company',
       '',
       'Customer_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('trg_test_sat_customer',
       'core.dbo.S_Customer',
       'Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company',
       '',
       'Customer_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('src_test_link_customer_address',
       'SELECT  HASHBYTES(\'\'SHA1\'\', CONCAT(CAST(Cust.CID as nvarchar),CAST(adr.AID as nvarchar))) as CustomerAddress_HK,
        HASHBYTES(\'\'SHA1\'\', CAST(Cust.CID as nvarchar)) as Customer_HK,
        HASHBYTES(\'\'SHA1\'\', CAST(adr.AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID',
       'CustomerAddress_HK,Customer_HK,Address_HK,LoadDatetime,SourceSystem',
       '',
       'CustomerAddress_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('trg_test_link_customer_address',
       'core.dbo.L_CustomerAddress',
       'CustomerAddress_HK,Customer_HK,Address_HK,LoadDatetime,SourceSystem',
       '',
       'CustomerAddress_HK');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('src_test_scd2',
       'SELECT   Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK',
       'Customer_ID,Name,BirthDate,Company',
       'Name,BirthDate,Company',
       'Customer_ID');

INSERT INTO ETL_MAP_TABLES(NAME,DEFINITION,COLUMNS,SCD_COLUMNS,COMPARE_KEY) 
values('trg_test_scd2',
       'mart.dbo.Customer',
       'Customer_ID,Name,BirthDate,Company',
       'Name,BirthDate,Company',
       'Customer_ID');

INSERT INTO ETL_MAP(SRC_ID,TRG_ID,KM_ID) values(1,2,1);
INSERT INTO ETL_MAP(SRC_ID,TRG_ID,KM_ID) values(3,4,2);
INSERT INTO ETL_MAP(SRC_ID,TRG_ID,KM_ID) values(5,6,3);
INSERT INTO ETL_MAP(SRC_ID,TRG_ID,KM_ID) values(7,8,4);

INSERT INTO ETL_OPTIONS(NAME,TYPE,DEFAULT_VALUE,DESCRIPTION) VALUES ('debug','bool','1','debug flag');

commit;