from sqlalchemy.orm import Session
from core.parser import Parser
from core.km_api import *
from model.tables import etl_hist
from datetime import datetime
from core.lazy_dict import LazyDict
from exc.BadApiCallError import *
from exc.MissingValueError import MissingValueError
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('ETLPreter')
handler = logging.FileHandler('ETLPreter.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class ETLPreter():
    """
        Class handling whole interpreting process.
    """

    def __init__(self):
        self.now = datetime.now()

    def __interpret_call(self, call, row):
        argument_parser = Parser('(', ')')
        start_call, end_call, start_arg, end_arg = argument_parser.find_tokens(
            call)

        arg = ''

        if (len(min(start_arg, end_arg)) > 0):
            arg = call[start_arg[0]:end_arg[0]]
            call = call[:start_arg[0]-1]

        call = call.upper()
        if (call == ''):
            return ''

        logger.info('Interpreting call: '+call)

        KM_API_DICT = LazyDict({
            'GET.SRC.DEFINITION': (KM_Api().get_etl_map_tables_definition, [row.src]),
            'GET.TRG.DEFINITION': (KM_Api().get_etl_map_tables_definition, [row.trg]),
            'GET.SRC.COLUMNS': (KM_Api().get_etl_map_tables_columns, [row.src, arg]),
            'GET.TRG.COLUMNS': (KM_Api().get_etl_map_tables_columns, [row.trg, arg]),
            'GET.SCD_COLUMNS': (KM_Api().get_scd_columns, [row, arg]),
            'GET.SRC.COMPARE_KEY': (KM_Api().get_etl_map_tables_compare_key, [row.src]),
            'GET.TRG.COMPARE_KEY': (KM_Api().get_etl_map_tables_compare_key, [row.trg]),
            'GET.TMP.SRC.NAME': (KM_Api().get_tmp_name, [row.src, row]),
            'GET.TMP.TRG.NAME': (KM_Api().get_tmp_name, [row.trg, row]),
            'GET.TMP.SRC.COLUMNS': (KM_Api().get_etl_map_tables_columns, [row.src, KM_Api().get_tmp_name(row.src, row)]),
            'GET.TMP.TRG.COLUMNS': (KM_Api().get_etl_map_tables_columns, [row.trg, KM_Api().get_tmp_name(row.trg, row)]),
            'GET.ETL.OPTION': (KM_Api().get_etl_option, [row, arg]),
        })

        try:
            func = KM_API_DICT.get(call)
        except MissingValueError as e:
            logger.exception('Missing value: '+e.attribute +
                             ' in call '+call+' from ' + str(row.id))
            raise

        if (func is None):
            logger.exception('Bad API call: '+call)
            raise BadApiCallError(
                call, 'Unknown API call in etl_knowledge_module.ID: '+str(row.km.id))

        logger.info('Call interpreted as: '+func)
        return func

     

    def interpret(self, row):
        """
            Method proccessing the interpretation. 

            Args:
            row(etl_map class): row in ETL_MAP table

            Returns:
            list: a list of etl_hist class - batch of ETL_HIST rows
        """
        logger.info('Start of interpreting etl_map.id:'+str(row.id))
        etl_hist_list = []
        interpreted = []
        steps = []
        km = row.km

        for step in km.ETL_KM_STEPS:
            # add attributes values to etl_hist
            new_hist = etl_hist()
            new_hist.etl_map_id = row.id
            new_hist.etl_km_id = km.id
            new_hist.etl_km_steps_id = step.id
            new_hist.loaddatetime = self.now
            etl_hist_list.append(new_hist)

            # vyber vsetky stepy do listu steps
            steps.append(step.step)

        parser_step = Parser('<%', '%>')

        for step in steps:
            # find substrung positions
            start_static, end_static, start_call, end_call = parser_step.find_tokens(
                step)

            static = [step[start_static[i]:end_static[i]]
                      for i in range(len(start_static))]
            calls = [step[start_call[i]:end_call[i]]
                     for i in range(len(start_call))]

            called = []
            # call api interpretation of step
            for call in calls:

                # parser_call = Parser('.','.')
                # start_api, end_api, start_delimiter, end_delimiter = parser_call.find_tokens(call)
                # api_call = [call[start_api[i]:end_api[i]] for i in range(len(start_api))]
                called.append(self.__interpret_call(call, row))

            interpreted_step = ''
            if (called == []):
                interpreted_step = static[0]

            for i in range(len(min(static, called))):
                interpreted_step += static[i]+called[i]

            interpreted.append(interpreted_step)

        for i in range(len(interpreted)):
            etl_hist_list[i].etl_km_step = interpreted[i].replace('\\n', '\n')

        return etl_hist_list
