from model.tables import etl_options
from core.parser import Parser
from hashlib import sha1
from model.database import Database
from datetime import datetime
from exc.MissingValueError import MissingValueError


class KM_Api():
    """Class for API calls from knowledge modules."""

    def get_etl_map_tables_definition(self, table):
        """
            API call for definition

            Args:
            table(etl_map_tables): row from ETL_MAP_TABLES table

            Returns:
            string: definition of src select/trg table
        """
        if(table.definition is None):
            raise MissingValueError(
                'definition', 'Missing value in src or trg etl_map_tables.ID: '+str(table.id))

        return table.definition.strip()

    def get_etl_map_tables_columns(self, table, alias):
        """
            API call for columns

            Args:
            table(etl_map_tables): row from ETL_MAP_TABLES table
            alias(string): optional to aliase columns, example: 'alias.column1', 'alias.column2', ...

            Returns:
            string: columns of src columns/trg columns
        """
        if(alias == ''):
            return table.columns.strip()

        if(table.columns is None):
            raise MissingValueError(
                'columns', 'Missing value in src or trg etl_map_tables.ID: '+str(table.id))

        parser = Parser(',', ',')
        start_col_src, end_col_src, start_comma, end_comma = parser.find_tokens(
            table.columns)
        src_cols = [table.columns[start_col_src[i]:end_col_src[i]]
                    for i in range(len(start_col_src))]

        aliased_src_cols = ''
        for i in range(len(src_cols)-1):
            aliased_src_cols += alias+'.'+src_cols[i].strip() + ', '

        aliased_src_cols += alias+'.'+src_cols[-1].strip()
        return aliased_src_cols

    def get_etl_map_tables_compare_key(self, table):
        """
            API call for compare_key

            Args:
            table(etl_map_tables): row from ETL_MAP_TABLES table

            Returns:
            string: compare_key of src compare_key/trg compare_key
        """
        if(table.compare_key is None):
            raise MissingValueError(
                'compare_key', 'Missing value in src or trg etl_map_tables.ID: '+str(table.id))

        return table.compare_key.strip()

    def get_scd_columns(self, row, alias_src_trg):
        """
            API call for scd_columns. Combines src and trg columns to create comparison. column1_src <> column1_trg

            Args:
            row(etl_map): row from ETL_MAP table

            Returns:
            string: scd_columns comparison 
        """
        parser = Parser(',', ',')

        if(row.src.scd_columns is None) or (row.trg.scd_columns is None):
            raise MissingValueError(
                'scd_columns', 'Missing value in src or trg etl_map.ID: '+str(row.id))

        start_col_src, end_col_src, start_comma, end_comma = parser.find_tokens(
            row.src.scd_columns)
        start_col_trg, end_col_trg, start_comma, end_comma = parser.find_tokens(
            row.trg.scd_columns)

   
        start_arg, end_arg, start_comma, end_comma = parser.find_tokens(
            alias_src_trg)
        
        args = ['','']

        if (len(min(start_arg, start_arg)) > 0):
            for i in range(len(min(start_arg, end_arg))):
                args[i] = alias_src_trg[start_arg[i]:end_arg[i]]
        

        src_cols = [row.src.scd_columns[start_col_src[i]:end_col_src[i]]
                    for i in range(len(start_col_src))]
        trg_cols = [row.trg.scd_columns[start_col_trg[i]:end_col_trg[i]]
                    for i in range(len(start_col_trg))]

        scd_cols = ''
        
        if(args[0] != ''):
            for i in range(len(src_cols)):
                src_cols[i] = args[0] + '.' + src_cols[i].strip()
        if(args[1] != ''):
            for i in range(len(trg_cols)):
                trg_cols[i] = args[1] + '.' + trg_cols[i].strip()

        for i in range(min(len(src_cols), len(trg_cols))-1):
            scd_cols += src_cols[i].strip() + ' <> ' + trg_cols[i].strip() + ' OR '

        scd_cols += src_cols[-1].strip()+' <> ' + trg_cols[-1].strip()
        return scd_cols

    def get_tmp_name(self, table, row):
        """
            API call for tmp name. Creates uniqe tmp name combining row id of etl_map and columns of etl_map_tables

            Args:
            table(etl_map_tables): row from ETL_MAP_TABLES table
            row(etl_map): row from ETL_MAP table

            Returns:
            string:  hashed string in hexa
        """
        string = str(table.id) + \
            str(table.name) + \
            str(table.definition) + \
            str(table.columns) + \
            str(table.compare_key) + \
            str(table.scd_columns) + \
            str(row.id)

        string_encoded = string.encode()
        return '#'+sha1(string_encoded).hexdigest()

    def get_etl_option(self, row, opt_name):
        """
            API call for etl options. If knowledge module has the option - opt_name - defined, it returns the defined value.
            Otherwise default value from table ETL_OPTIONS

            Args:
            row(etl_map): row from ETL_MAP table
            opt_name(string): name of the option in tables ETL_OPTIONS & ETL_KM_OPTIONS

            Returns:
            string: opt_name value
        """
        km = row.km
        km_options = []

        km_options = [km.ETL_KM_OPTIONS]
        connection = Database()
        session = connection.connect()

        value = ''
        if not(km_options[0] is None):
            for opt in km_options:
                if (opt.opt.name.upper() == opt_name.upper()):
                    return opt.value

        value = session.query(etl_options).filter(
            etl_options.name == opt_name)

        if(value[0] is None):
            raise MissingValueError(opt_name, 'Missing option in etl_options')

        etl_opt = value[0]
        return etl_opt.default_value
