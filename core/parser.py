class Parser():
    """
    Class to parse API calls from knowledge modules but it become handy to other uses, eg. when the same
    token is used as left and right it can parse columns from string where the columns are separated by that token
    """

    def __init__(self, left_token, right_token):
        self.left_token = left_token
        self.right_token = right_token
        self.len_left_token = len(left_token)
        self.len_right_token = len(right_token)

    def find_tokens(self, step):
        """
        Return lists are same in length if there is something to parse, otherwise call lists are empty.
        Same length of the lists is reached by adding dummy to both start and and lists of appropriate part.
        Eg. substring of x='abc' x[0:0] is ''(empty string), also substring x[100:100] is ''

        Args:
        step(string): string to parse

        Returns
        list: start_static - list of indexes where static (part of the string which is intended to stay the same) text starts
        list: end_static - list of indexes where static text ends +1, so it could be retrieved by indexing substrings (x[:])
        list: start_call - list of indexes where parsed call starts
        list: end_call - list of indexes where parsed call ends +1, same reason as end_static
        """
        str = step
        start = 0
        offset = 0
        start_static, end_static, start_call, end_call = [], [], [], []

        while(str.find(self.left_token) != -1):

            start_static.append(start+offset)

            position = str.find(self.left_token)
            start_call.append(position + self.len_left_token + offset)
            end_static.append(position + offset)

            position = str.find(self.right_token)
            end_call.append(position + offset)

            start = position+self.len_right_token

            str = str[start:]
            offset = offset + start
            start = 0

        if(len(min(start_static, end_static, start_call, end_call)) == 0):
            return [0], [len(step)], [], []

        if (offset < len(step)):
            start_static.append(offset)
            end_static.append(len(step))

            start_call.append(len(step))
            end_call.append(len(step))

        return start_static, end_static, start_call, end_call
