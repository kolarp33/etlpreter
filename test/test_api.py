import unittest
from core.km_api import *
from model.tables import *
from exc.MissingValueError import *


class TestAPI(unittest.TestCase):
    map_tables_src = etl_map_tables()
    map_tables_src.id = 1
    map_tables_src.columns = 'test1, test2'
    map_tables_src.compare_key = 'test_key'
    map_tables_src.definition = 'select * from test.test'
    map_tables_src.name = 'api_unittest src'
    map_tables_src.scd_columns = 'test1_src, test2_src'

    map_tables_trg = etl_map_tables()
    map_tables_trg.id = 2
    map_tables_trg.columns = 'test1_trg, test2_trg  '
    map_tables_trg.compare_key = 'test_key_trg'
    map_tables_trg.definition = 'test_schema.test_table   '
    map_tables_trg.name = 'api_unittest trg'
    map_tables_trg.scd_columns = 'test1_trg, test2_trg  '

    api = KM_Api()

    etl_knowledge_module = etl_knowledge_module()
    etl_knowledge_module.id = 1
    etl_knowledge_module.name = 'api_test'
    etl_knowledge_module.version = 1

    elt_map = etl_map()
    etl_map.id = 1
    etl_map.src = map_tables_src
    etl_map.trg = map_tables_trg
    etl_map.km = etl_knowledge_module

    etl_options = etl_options()
    etl_options.id = 1
    etl_options.name = 'api_test'
    etl_options.type = 'Boolean'
    etl_options.default_value = 'False'

    def test_definition(self):
        self.assertEqual(self.api.get_etl_map_tables_definition(
            self.map_tables_src), 'select * from test.test')

        self.assertEqual(self.api.get_etl_map_tables_definition(
            self.map_tables_trg), 'test_schema.test_table')

        self.map_tables_src.definition = None

    def test_columns_without_alias(self):
        self.assertEqual(self.api.get_etl_map_tables_columns(
            self.map_tables_src, ''), 'test1, test2')
        self.assertEqual(self.api.get_etl_map_tables_columns(
            self.map_tables_trg, ''), 'test1_trg, test2_trg')

    def test_columns_with_alias(self):
        self.assertEqual(self.api.get_etl_map_tables_columns(
            self.map_tables_src, 'src'), 'src.test1, src.test2')
        self.assertEqual(self.api.get_etl_map_tables_columns(
            self.map_tables_trg, 'trg'), 'trg.test1_trg, trg.test2_trg')

    def test_compare_keys(self):
        self.assertEqual(self.api.get_etl_map_tables_compare_key(
            self.map_tables_src), 'test_key')
        self.assertEqual(self.api.get_etl_map_tables_compare_key(
            self.map_tables_trg), 'test_key_trg')

    def test_scd_columns(self):
        self.assertEqual(self.api.get_scd_columns(etl_map,''),
                         'test1_src <> test1_trg OR test2_src <> test2_trg')
        self.assertEqual(self.api.get_scd_columns(etl_map,'src,trg'),
                         'src.test1_src <> trg.test1_trg OR src.test2_src <> trg.test2_trg')
        self.assertEqual(self.api.get_scd_columns(etl_map,',trg'),
                         'test1_src <> trg.test1_trg OR test2_src <> trg.test2_trg')
        self.assertEqual(self.api.get_scd_columns(etl_map,'src'),
                         'src.test1_src <> test1_trg OR src.test2_src <> test2_trg')

    def test_options(self):
        # default value look up from database
        # self.assertEqual(self.api.get_etl_option(etl_map, 'api_test'), 'False')

        km_options = etl_km_options()
        km_options.id = 1
        km_options.value = 'True'
        km_options.km = self.etl_knowledge_module
        km_options.opt = self.etl_options

        self.assertEqual(self.api.get_etl_option(etl_map, 'api_test'), 'True')

    if __name__ == '__main__':
        unittest.main()
