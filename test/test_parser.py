import unittest
from core.parser import Parser


class TestParser(unittest.TestCase):

    t_string1 = 'abc <%aaa%> '
    t_string2 = '<%aaa%> abc'
    t_string3 = 'abc'
    parser = Parser('<%', '%>')

    def test_boundaries(self):
        start_static, end_static, start_call, end_call = [], [], [], []
        start_static, end_static, start_call, end_call = self.parser.find_tokens(
            self.t_string1)

        self.assertEqual(start_static, [0, 11])
        self.assertEqual(end_static, [4, 12])
        self.assertEqual(start_call, [6, 12])
        self.assertEqual(end_call, [9, 12])

        start_static, end_static, start_call, end_call = self.parser.find_tokens(
            self.t_string2)

        self.assertEqual(start_static, [0, 7])
        self.assertEqual(end_static, [0, 11])
        self.assertEqual(start_call, [2, 11])
        self.assertEqual(end_call, [5, 11])

        start_static, end_static, start_call, end_call = self.parser.find_tokens(
            self.t_string3)

        self.assertEqual(start_static, [0])
        self.assertEqual(end_static, [3])
        self.assertEqual(start_call, [])
        self.assertEqual(end_call, [])

    if __name__ == '__main__':
        unittest.main()
