﻿IF (OBJECT_ID('stage.dbo.Cust') IS NOT NULL) 
	DROP TABLE stage.dbo.Cust;
IF (OBJECT_ID('stage.dbo.Addr') IS NOT NULL) 
	DROP TABLE stage.dbo.Addr;

/*
DROP DATABASE Stage;
GO
*/

CREATE DATABASE Stage
GO

CREATE TABLE stage.dbo.Cust (
  CID int,
  FirstName varchar(100),
  LastName varchar(100),
  BirthDate varchar(8),
  Company varchar(255)
);



CREATE TABLE stage.dbo.Addr (
  AID int,
  CID int,
  Street varchar(255),
  City varchar(255),
  Country varchar(100),
  Zip varchar(10) 
);

INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (100,'Sopoline','Tanner','19540603','Sed Est Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (101,'Claudia','Nolan','20150303','Leo Cras Vehicula LLC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (102,'Dara','Rose','19920925','Donec Non Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (103,'Mari','Hayes','19590121','Nisl Sem Consequat LLC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (104,'Duncan','Morton','19890525','Neque Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (105,'Ria','Greene','19570126','Suspendisse Commodo Tincidunt Corporation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (106,'Mark','Turner','20130310','Pharetra Nibh Aliquam LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (107,'Jasmine','Pratt','19790907','Dapibus Id Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (108,'Beck','West','19780308','Consectetuer Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (109,'Winifred','Ellison','19840628','Justo Sit Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (110,'Vladimir','Clements','19930417','Cras Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (111,'Suki','Hamilton','19840327','Nunc Mauris Ltd');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (112,'Liberty','Travis','19700527','Dictum Cursus Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (113,'Drake','Osborne','20190929','Fames Ac PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (114,'Azalia','Dotson','19520627','Fusce Aliquet Magna Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (115,'McKenzie','Fields','20041111','Arcu Vestibulum Ante Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (116,'Xena','Chavez','19661127','Vel Venenatis Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (117,'Gabriel','Barry','19610402','Non Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (118,'Michael','Roach','19911014','Est Nunc Laoreet Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (119,'Sarah','Sullivan','20151219','Orci Company');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (120,'Bruno','Cook','20010331','Ultricies Dignissim Company');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (121,'Keelie','Rice','20041229','Neque Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (122,'Jin','Glass','19990415','Aliquet Diam Sed Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (123,'Destiny','Foster','19970529','Risus Quisque Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (124,'Walter','Carter','20001102','Quisque LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (125,'Mona','Andrews','20190919','Erat Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (126,'Kessie','Branch','19661024','Netus Et Malesuada Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (127,'Pamela','Newman','20080221','Ac Orci Ut Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (128,'Nevada','Ferrell','19890215','Sociis PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (129,'Kirestin','Peck','19710129','Eleifend Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (130,'Honorato','Lewis','19720228','Metus Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (131,'Kirk','Edwards','19910116','At LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (132,'Rina','Lindsey','19560104','Diam Sed Diam PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (133,'Brady','Hicks','20040331','Ac Mattis Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (134,'Duncan','Small','20051102','Et Ultrices Posuere PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (135,'Serena','Wolfe','19840217','A Facilisis Non Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (136,'Karly','Barry','19590915','Est Ac Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (137,'Channing','Burks','19900421','Bibendum Corporation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (138,'Norman','Lamb','20070708','Penatibus Et Magnis Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (139,'Giselle','Clemons','19910405','Dolor Vitae Dolor Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (140,'Joel','Patel','19940519','Eu Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (141,'Samson','Hawkins','20021127','Eget Varius Limited');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (142,'Andrew','Williamson','19950710','Pede Et Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (143,'Rafael','Gould','19900610','Urna Justo Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (144,'Griffith','Sanchez','19990804','Integer Vitae Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (145,'Hiroko','Jensen','19650526','Facilisis Vitae Corporation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (146,'Deacon','Snow','20080311','Dictum Placerat Augue PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (147,'Bethany','Gray','19941209','Consectetuer Cursus Et PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (148,'Ashely','Wong','19910513','Sociosqu Ad Litora Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (149,'Mikayla','Obrien','20190724','Eros Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (150,'Donovan','Gallagher','19781023','Eget Dictum Placerat Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (151,'Larissa','Guerra','19840426','Orci Adipiscing LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (152,'Reagan','Hebert','19811202','Euismod PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (153,'Bree','Mills','19940605','Commodo Ipsum Suspendisse Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (154,'Damian','Weber','20150221','Neque PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (155,'Yasir','Haney','19740713','Est Mollis Non Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (156,'Grace','Nicholson','19620528','At Pretium Aliquet PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (157,'Melyssa','Mcbride','19740503','Sed Pede Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (158,'Hyacinth','Ochoa','20021009','Aliquam Ultrices Ltd');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (159,'Amal','Ramsey','19760525','Ornare Placerat Company');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (160,'Lee','Larsen','19771218','At Auctor Ullamcorper Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (161,'Malik','Franco','19601115','Integer Eu Lacus Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (162,'Demetria','Ochoa','19640802','Enim Diam Vel Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (163,'Maya','Witt','19780511','Consectetuer Euismod LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (164,'Joseph','Snider','19701020','Aliquam PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (165,'Maile','Hancock','20181225','Blandit At Company');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (166,'Sonia','Pena','19850207','Molestie Dapibus Ligula Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (167,'Ulysses','Richmond','20090914','Velit Egestas Lacinia Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (168,'Logan','Pruitt','19670330','Felis Purus Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (169,'Cedric','Roberson','19770720','Ac Mattis Ornare LLC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (170,'Miriam','Stone','19500905','Semper Et LLC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (171,'Xanthus','Harmon','19691002','Donec Nibh PC');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (172,'Connor','Harding','19610419','Dui Semper Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (173,'Marsden','Glenn','20030821','Montes Institute');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (174,'Jesse','Livingston','19900302','Sed Et Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (175,'Cairo','Hampton','19960109','Ac Eleifend Vitae LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (176,'Galena','Knox','19951113','A Nunc In Limited');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (177,'Basil','Baker','19770124','Risus Quisque Libero Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (178,'Dalton','Mcintyre','20081109','Quam Elementum At Limited');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (179,'MacKensie','Hess','20140711','Mauris Morbi Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (180,'Hilel','Orr','19761225','In Faucibus Morbi Ltd');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (181,'Kim','Lara','20070426','Donec Limited');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (182,'Kennedy','Hammond','19510929','At Pretium Aliquet Foundation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (183,'Calvin','Williamson','19510304','Massa Suspendisse Eleifend Corporation');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (184,'Xena','Morton','20160225','Fringilla Est Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (185,'Hector','Mcclain','19960715','Eros Turpis Non Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (186,'Chelsea','Montgomery','19950504','Aliquam Erat Company');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (187,'Cassandra','Fischer','19781014','A Sollicitudin Orci Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (188,'Leah','Garrison','20100524','Molestie Dapibus Ltd');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (189,'Aladdin','Erickson','19581121','Risus Odio Limited');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (190,'Rhiannon','Livingston','19640522','Natoque Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (191,'Pascale','Eaton','19980213','Dolor Elit Pellentesque Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (192,'Charles','Jordan','19781029','Mus Aenean Eget Incorporated');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (193,'Austin','Fitzpatrick','20110506','Ornare In Associates');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (194,'Tobias','Cline','19511111','Habitant Morbi Consulting');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (195,'Bree','Shaffer','19510429','Montes Nascetur Industries');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (196,'Basil','Ramos','19550908','Cras Vehicula Aliquet Corp.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (197,'Wendy','Cochran','20151017','Magnis Dis Inc.');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (198,'Uta','Mann','19920728','Eros LLP');
INSERT INTO stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (199,'Blake','Ingram','19980112','Etiam Consulting');

INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1000,157,'P.O. Box 990, 6563 Sit Av.','Sint-Genesius-Rode','American Samoa','9891');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1001,158,'P.O. Box 923, 9414 Ornare Ave','Lac-Serent','Cambodia','688336');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1002,107,'P.O. Box 359, 9733 Nullam Road','Driekapellen','Uruguay','3784');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1003,114,'Ap #797-5073 Et St.','Salamanca','Nauru','30987-971');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1004,199,'P.O. Box 669, 5761 Ullamcorper St.','Freiburg','Bermuda','110334');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1005,145,'P.O. Box 758, 8779 Congue. Road','Oderzo','Cyprus','3034');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1006,108,'P.O. Box 916, 1834 Sapien Street','Nieuwegein','India','297273');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1007,132,'406-7131 Lacus. Ave','Brandon','Bonaire, Sint Eustatius and Saba','81-079');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1008,172,'P.O. Box 616, 9683 Donec St.','Louisville','Ghana','79-908');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1009,123,'943-9288 Aliquam Road','Saint-Prime','Botswana','34735');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1010,152,'Ap #538-1012 Amet Road','Saltara','Seychelles','28-103');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1011,167,'Ap #824-1521 Eleifend, Ave','Hay River','United States Minor Outlying Islands','80-586');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1012,143,'101-828 Sagittis Ave','Kansas City','Mayotte','310015');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1013,171,'Ap #949-2827 Donec Rd.','Varendonk','Falkland Islands','5718 XU');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1014,198,'P.O. Box 144, 8144 Eu Ave','Castellina in Chianti','Macao','66746');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1015,179,'P.O. Box 245, 5576 Dui. St.','Banff','United States Minor Outlying Islands','18708');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1016,110,'P.O. Box 113, 676 Consectetuer Av.','Lake Cowichan','Congo, the Democratic Republic of the','427785');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1017,108,'4417 Odio Road','Caplan','Indonesia','6659');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1018,123,'160-6579 Cras Avenue','Dolembreux','Syria','46646');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1019,160,'Ap #441-2067 Eget St.','Tierra Amarilla','Cayman Islands','58725');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1020,135,'3923 Orci Rd.','Manisa','Comoros','275752');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1021,104,'4760 Eget St.','Villafalletto','Niger','851880');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1022,144,'1765 Integer St.','Bhiwani','Andorra','12663');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1023,189,'454-1386 Magna. Rd.','Mobile','Azerbaijan','1925');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1024,153,'P.O. Box 344, 3781 Donec Street','Bury St. Edmunds','Afghanistan','833324');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1025,194,'2247 In St.','Stockport','Gabon','889343');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1026,164,'Ap #413-4357 Tincidunt Avenue','Alto Biobío','Colombia','52033');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1027,143,'P.O. Box 849, 9779 Donec St.','Buckingham','Liechtenstein','61408');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1028,140,'8882 Suspendisse Av.','Chippenham','Kuwait','7632');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1029,198,'P.O. Box 458, 9359 Egestas. St.','Lautaro','Honduras','M1R 1Z6');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1030,175,'7327 Donec Rd.','Tranent','Honduras','82953-812');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1031,198,'6870 Phasellus Rd.','Kawerau','Slovenia','93-729');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1032,151,'P.O. Box 828, 8354 Non Road','Cobquecura','New Zealand','48626');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1033,183,'Ap #656-1605 Duis St.','Opwijk','Yemen','T5K 8HD');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1034,141,'Ap #903-8763 Urna. Ave','Luziânia','Guernsey','N5Z 9W1');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1035,138,'Ap #963-7489 Eu Av.','Bodmin','Belarus','98936');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1036,154,'P.O. Box 295, 9533 Morbi Av.','Pak Pattan','Iceland','V9S 1R4');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1037,192,'Ap #856-9083 At, Road','Hoeilaart','Senegal','T6 4NA');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1038,103,'Ap #276-4725 Libero. Road','Stamford','Gambia','429183');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1039,124,'P.O. Box 616, 2052 Eu Avenue','Langen','Norfolk Island','16465');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1040,164,'P.O. Box 486, 3765 Curabitur Ave','Koningshooikt','Italy','8065');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1041,168,'Ap #436-765 Neque St.','Montbéliard','Central African Republic','9827');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1042,160,'304-4810 Nec Road','Marche-les-Dames','Reunion','48262');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1043,147,'P.O. Box 930, 2797 Non, Rd.','Orai','Russian Federation','88917');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1044,117,'596-8023 Aliquet Av.','Valparaíso','Nepal','837933');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1045,187,'P.O. Box 513, 3492 Aliquam Rd.','St. John''s','Puerto Rico','23374');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1046,154,'6220 Ornare, St.','Valparaíso de Goiás','Curaçao','19940');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1047,163,'851-5359 Vitae Ave','Bafra','Poland','1624');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1048,124,'P.O. Box 685, 8288 Torquent Avenue','Chañaral','Isle of Man','0495 KR');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1049,120,'4359 Quam Ave','Kingussie','Spain','22714');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1050,136,'2490 Vel Street','San Marcello','Gabon','790066');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1051,107,'Ap #653-9452 Eu Road','Suwałki','Maldives','6953');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1052,165,'590-1744 Cursus Av.','Ghaziabad','Paraguay','59871');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1053,136,'Ap #760-9270 Quis Av.','Red Deer','Montenegro','1983');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1054,198,'Ap #669-7973 Magnis Ave','Roermond','Haiti','635665');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1055,119,'Ap #384-8045 Quisque Avenue','Potsdam','Macao','82890');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1056,126,'531 Et, Rd.','Rae Lakes','Germany','15485');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1057,128,'P.O. Box 104, 8991 Ridiculus St.','Dorchester','Nicaragua','5332');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1058,168,'Ap #218-8275 Lorem Rd.','Flawinne','French Polynesia','70919');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1059,157,'P.O. Box 924, 527 Odio St.','Lafayette','Australia','13-855');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1060,181,'Ap #194-5238 Sodales St.','Winterswijk','Luxembourg','15-432');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1061,193,'P.O. Box 489, 8449 Ac Road','Mansfield-et-Pontefract','Saint Kitts and Nevis','92571');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1062,129,'9209 Eget St.','Santa Cruz de Tenerife','Luxembourg','37779');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1063,189,'P.O. Box 562, 2534 Et Avenue','Atlanta','Puerto Rico','779801');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1064,166,'9615 Erat, Avenue','Broken Arrow','Zambia','6843');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1065,120,'Ap #727-2331 Neque Av.','Buggenhout','Yemen','598594');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1066,167,'7625 Proin Rd.','San Isidro de El General','Saudi Arabia','39616');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1067,166,'Ap #830-7329 Est Av.','Mandi Burewala','Kyrgyzstan','539623');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1068,172,'P.O. Box 183, 7338 Lacus. Street','Porvenir','Korea, South','2839');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1069,100,'7970 At, St.','Argyle','United States Minor Outlying Islands','9968');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1070,109,'782-2347 Phasellus Road','Devon','Kyrgyzstan','759588');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1071,193,'776-1393 Tellus. Avenue','Poulseur','Tuvalu','84313');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1072,176,'P.O. Box 859, 1647 At Avenue','Owerri','Trinidad and Tobago','79878');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1073,173,'Ap #983-2332 Dictum Road','Halkirk','Albania','61217');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1074,127,'2198 Ante, St.','St. John''s','Germany','2698');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1075,189,'P.O. Box 833, 5805 Nibh. Street','Brahmapur','Isle of Man','0899 SR');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1076,124,'6850 Quisque Ave','Argyle','Belize','71536');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1077,177,'P.O. Box 385, 8854 Diam Rd.','Petrolina','South Georgia and The South Sandwich Islands','0551 AL');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1078,197,'975-3632 Malesuada Rd.','Lauw','Congo (Brazzaville)','2489');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1079,164,'Ap #244-7034 Donec Ave','Siculiana','Jordan','400895');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1080,152,'P.O. Box 822, 7518 Ut Avenue','Spruce Grove','French Guiana','16949');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1081,116,'P.O. Box 327, 1722 Odio, Rd.','Cargovil','Northern Mariana Islands','D1 8GC');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1082,126,'P.O. Box 608, 4686 A St.','Chelmsford','Senegal','68076-195');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1083,168,'Ap #457-4412 Lacus. Road','Balurghat','Turks and Caicos Islands','57426');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1084,136,'722-5688 Sollicitudin Street','La Matap�dia','Seychelles','D58 0DG');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1085,148,'880-2110 Nulla Av.','Punta Arenas','Brazil','05630');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1086,161,'P.O. Box 436, 3276 Semper Road','Brye','Macedonia','20134-156');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1087,197,'7718 Curae; Rd.','Los Andes','Chad','03483');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1088,162,'Ap #672-8385 Ligula St.','Wilmont','Croatia','589442');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1089,125,'Ap #528-3708 Ut Avenue','Sint-Denijs','Montenegro','3970 FD');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1090,113,'Ap #227-4507 Feugiat. St.','Dumfries','Romania','456117');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1091,107,'Ap #330-8201 Penatibus Ave','L�vis','United Arab Emirates','TR0N 6MF');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1092,112,'Ap #302-5175 Lorem Ave','Hamilton','Peru','27017');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1093,123,'723-1360 Posuere, Ave','Wichelen','Bulgaria','38249');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1094,168,'P.O. Box 375, 9782 Aliquam Av.','Ligney','Cayman Islands','8089');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1095,106,'P.O. Box 429, 3208 Non, St.','Mulchén','Comoros','651442');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1096,130,'235-6841 Lacus. St.','Trochu','Swaziland','14826');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1097,132,'713 Eu Av.','Dilbeek','Moldova','827541');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1098,197,'P.O. Box 516, 1324 Proin Av.','Vancouver','Cape Verde','P1G 9A3');
INSERT INTO stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (1099,150,'272-3715 Dignissim Road','Neustadt am Rübenberge','Lithuania','16042');
