SELECT   Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK