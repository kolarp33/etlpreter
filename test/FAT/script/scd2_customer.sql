/*prazdna cielova tabulka*/
truncate table  mart.dbo.Customer 

/*pocet dat na zdroji*/
select count(*) as Src
from (SELECT   Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK) src


/*spustenie vygenerovanej procedury*/
exec(
'IF (OBJECT_ID(''tempdb..#0903fb870dbb78b48bbfb56e3827d90210443350'') IS NOT NULL) 
    DROP TABLE #0903fb870dbb78b48bbfb56e3827d90210443350;

select * 
INTO #0903fb870dbb78b48bbfb56e3827d90210443350
from (SELECT  Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK) src

INSERT INTO mart.dbo.Customer
SELECT
    Customer_ID,Name,BirthDate,Company
    ,getdate() as ValidFrom
    ,''30000101'' as ValidTo
    ,1 as CurrentFlag
FROM
(
    MERGE mart.dbo.Customer AS Target
    USING #0903fb870dbb78b48bbfb56e3827d90210443350 AS Source
    ON Source.Customer_ID = Target.Customer_ID AND Target.CurrentFlag = 1

    WHEN MATCHED AND
    (
        Source.Name <> Target.Name OR Source.BirthDate <> Target.BirthDate OR Source.Company <> Target.Company
    )
    THEN UPDATE 
    SET
        Target.CurrentFlag = 0,
        Target.[ValidTo] = getdate()-1
    WHEN NOT MATCHED
    THEN INSERT
    (
        Customer_ID,Name,BirthDate,Company
        ,ValidFrom
        ,ValidTo
        ,CurrentFlag
    )
    VALUES
    (
        Source.Customer_ID, Source.Name, Source.BirthDate, Source.Company
        ,getdate()
        ,''30000101''
        ,1
    )
    OUTPUT $action AS Action, Source.* 
) as MergeOutput
WHERE MergeOutput.Action = ''UPDATE'';')


/*pocet dat v cielovej tabulke*/
select count(*) as Init from mart.dbo.Customer 

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#0903fb870dbb78b48bbfb56e3827d90210443350'') IS NOT NULL) 
    DROP TABLE #0903fb870dbb78b48bbfb56e3827d90210443350;

select * 
INTO #0903fb870dbb78b48bbfb56e3827d90210443350
from (SELECT  Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK) src

INSERT INTO mart.dbo.Customer
SELECT
    Customer_ID,Name,BirthDate,Company
    ,getdate() as ValidFrom
    ,''30000101'' as ValidTo
    ,1 as CurrentFlag
FROM
(
    MERGE mart.dbo.Customer AS Target
    USING #0903fb870dbb78b48bbfb56e3827d90210443350 AS Source
    ON Source.Customer_ID = Target.Customer_ID AND Target.CurrentFlag = 1

    WHEN MATCHED AND
    (
        Source.Name <> Target.Name OR Source.BirthDate <> Target.BirthDate OR Source.Company <> Target.Company
    )
    THEN UPDATE 
    SET
        Target.CurrentFlag = 0,
        Target.[ValidTo] = getdate()-1
    WHEN NOT MATCHED
    THEN INSERT
    (
        Customer_ID,Name,BirthDate,Company
        ,ValidFrom
        ,ValidTo
        ,CurrentFlag
    )
    VALUES
    (
        Source.Customer_ID, Source.Name, Source.BirthDate, Source.Company
        ,getdate()
        ,''30000101''
        ,1
    )
    OUTPUT $action AS Action, Source.* 
) as MergeOutput
WHERE MergeOutput.Action = ''UPDATE'';')


/*zachovanie povodneho poctu zaznamov bez delete/truncate*/
select count(*) as Reload from mart.dbo.Customer 

select top 5 * from mart.dbo.Customer 
order by BirthDate desc

update core.dbo.S_Customer
SET
BirthDate = '30420101'
where Customer_HK = 0x8739AB73E27D444291AAFE563DFBA8832AD5D722

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#0903fb870dbb78b48bbfb56e3827d90210443350'') IS NOT NULL) 
    DROP TABLE #0903fb870dbb78b48bbfb56e3827d90210443350;

select * 
INTO #0903fb870dbb78b48bbfb56e3827d90210443350
from (SELECT  Customer_ID
        ,Name
        ,BirthDate
        ,Company 
FROM core.dbo.H_Customer h
LEFT JOIN (select *
       	   from
      	   (select b.*, row_number() over(partition by Customer_HK order by b.LoadDatetime desc)rn 
			from core.dbo.S_Customer as b )s
      		where rn = 1) AS b 
ON h.Customer_HK = b.Customer_HK) src

INSERT INTO mart.dbo.Customer
SELECT
    Customer_ID,Name,BirthDate,Company
    ,getdate() as ValidFrom
    ,''30000101'' as ValidTo
    ,1 as CurrentFlag
FROM
(
    MERGE mart.dbo.Customer AS Target
    USING #0903fb870dbb78b48bbfb56e3827d90210443350 AS Source
    ON Source.Customer_ID = Target.Customer_ID AND Target.CurrentFlag = 1

    WHEN MATCHED AND
    (
        Source.Name <> Target.Name OR Source.BirthDate <> Target.BirthDate OR Source.Company <> Target.Company
    )
    THEN UPDATE 
    SET
        Target.CurrentFlag = 0,
        Target.[ValidTo] = getdate()-1
    WHEN NOT MATCHED
    THEN INSERT
    (
        Customer_ID,Name,BirthDate,Company
        ,ValidFrom
        ,ValidTo
        ,CurrentFlag
    )
    VALUES
    (
        Source.Customer_ID, Source.Name, Source.BirthDate, Source.Company
        ,getdate()
        ,''30000101''
        ,1
    )
    OUTPUT $action AS Action, Source.* 
) as MergeOutput
WHERE MergeOutput.Action = ''UPDATE'';')

/*pridanie jedneho zaznamu*/
select count(*) as Updated from mart.dbo.Customer

select top 5 * from mart.dbo.Customer 
order by BirthDate desc