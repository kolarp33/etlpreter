/*prazdna cielova tabulka*/
truncate table core.dbo.L_CustomerAddress

/*pocet dat na zdroji*/
select count(*) as Src
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID 

/*spustenie vygenerovanej procedury*/
exec(
'IF (OBJECT_ID(''tempdb..#6842667c43af2e43000f97ff44b6518e606e89ac'') IS NOT NULL) 
    DROP TABLE #6842667c43af2e43000f97ff44b6518e606e89ac;

select * 
INTO #6842667c43af2e43000f97ff44b6518e606e89ac
from (SELECT  HASHBYTES(''SHA1'', CONCAT(CAST(cust.CID as nvarchar),CAST(AID as nvarchar))) as CustomerAddress_HK,
        HASHBYTES(''SHA1'', CAST(cust.CID as nvarchar)) as Customer_HK,
        HASHBYTES(''SHA1'', CAST(AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.L_CustomerAddress
				(CustomerAddress_HK,Customer_HK,Address_HK,LoadDatetime,SourceSystem)
				SELECT  tmp.CustomerAddress_HK, tmp.Customer_HK, tmp.Address_HK, tmp.LoadDatetime, tmp.SourceSystem
				FROM #6842667c43af2e43000f97ff44b6518e606e89ac AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by CustomerAddress_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.L_CustomerAddress as b )s
      						where rn = 1) AS b  
				ON tmp.CustomerAddress_HK = b.CustomerAddress_HK 
				AND tmp.SourceSystem = b.SourceSystem
				WHERE b.CustomerAddress_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pocet dat v cielovej tabulke*/
select count(*) as Init from Core.dbo.L_CustomerAddress

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#6842667c43af2e43000f97ff44b6518e606e89ac'') IS NOT NULL) 
    DROP TABLE #6842667c43af2e43000f97ff44b6518e606e89ac;

select * 
INTO #6842667c43af2e43000f97ff44b6518e606e89ac
from (SELECT  HASHBYTES(''SHA1'', CONCAT(CAST(cust.CID as nvarchar),CAST(AID as nvarchar))) as CustomerAddress_HK,
        HASHBYTES(''SHA1'', CAST(cust.CID as nvarchar)) as Customer_HK,
        HASHBYTES(''SHA1'', CAST(AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.L_CustomerAddress
				(CustomerAddress_HK,Customer_HK,Address_HK,LoadDatetime,SourceSystem)
				SELECT  tmp.CustomerAddress_HK, tmp.Customer_HK, tmp.Address_HK, tmp.LoadDatetime, tmp.SourceSystem
				FROM #6842667c43af2e43000f97ff44b6518e606e89ac AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by CustomerAddress_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.L_CustomerAddress as b )s
      						where rn = 1) AS b  
				ON tmp.CustomerAddress_HK = b.CustomerAddress_HK 
				AND tmp.SourceSystem = b.SourceSystem
				WHERE b.CustomerAddress_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*zachovanie povodneho poctu zaznamov bez delete/truncate*/
select count(*) as Reload from Core.dbo.L_CustomerAddress

/*pridanie zaznamu na zdroji*/
INSERT INTO Stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (1000,'Test123','Garrison','30000101','Test123');
INSERT INTO Stage.dbo.Addr (AID,CID,Street,City,Country,Zip) VALUES (10000,1000,'Test 123 Av.','Test-Rode','Test Country','42');

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#6842667c43af2e43000f97ff44b6518e606e89ac'') IS NOT NULL) 
    DROP TABLE #6842667c43af2e43000f97ff44b6518e606e89ac;

select * 
INTO #6842667c43af2e43000f97ff44b6518e606e89ac
from (SELECT  HASHBYTES(''SHA1'', CONCAT(CAST(cust.CID as nvarchar),CAST(AID as nvarchar))) as CustomerAddress_HK,
        HASHBYTES(''SHA1'', CAST(cust.CID as nvarchar)) as Customer_HK,
        HASHBYTES(''SHA1'', CAST(AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.L_CustomerAddress
				(CustomerAddress_HK,Customer_HK,Address_HK,LoadDatetime,SourceSystem)
				SELECT  tmp.CustomerAddress_HK, tmp.Customer_HK, tmp.Address_HK, tmp.LoadDatetime, tmp.SourceSystem
				FROM #6842667c43af2e43000f97ff44b6518e606e89ac AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by CustomerAddress_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.L_CustomerAddress as b )s
      						where rn = 1) AS b  
				ON tmp.CustomerAddress_HK = b.CustomerAddress_HK 
				AND tmp.SourceSystem = b.SourceSystem
				WHERE b.CustomerAddress_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pridanie jedneho zaznamu*/
select count(*) as Inserted from Core.dbo.L_CustomerAddress

/*ukazka dat*/
select top 5 * from Core.dbo.L_CustomerAddress
order by loaddatetime desc

/*zmazanie pridanych testovacich dat*/
DELETE FROM Stage.dbo.Cust 
WHERE CID = 1000

DELETE FROM Stage.dbo.Addr 
WHERE AID = 10000




