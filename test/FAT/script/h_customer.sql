/*prazdna cielova tabulka*/
truncate table core.dbo.H_Customer

/*pocet dat na zdroji*/
select count(*) as Src
from (SELECT  *
	  FROM Stage.dbo.Cust) src

/*spustenie vygenerovanej procedury*/
exec(
'IF (OBJECT_ID(''tempdb..#58d3f9dc604f4d27c3d51077b93e30aaf4294508'') IS NOT NULL) 
    DROP TABLE #58d3f9dc604f4d27c3d51077b93e30aaf4294508;

select * 
INTO #58d3f9dc604f4d27c3d51077b93e30aaf4294508
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        CID as Customer_ID
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

		INSERT INTO core.dbo.H_Customer
		(Customer_HK,LoadDatetime,SourceSystem,Customer_ID)
		SELECT tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.Customer_ID
		FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 AS tmp
		LEFT JOIN 	(SELECT b.Customer_HK, b.SourceSystem
							FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 b
							JOIN core.dbo.H_Customer
								on b.Customer_HK = core.dbo.H_Customer.Customer_HK
								and b.SourceSystem = core.dbo.H_Customer.SourceSystem
					) b
		ON tmp.Customer_HK = b.Customer_HK AND tmp.SourceSystem = b.SourceSystem
		WHERE b.Customer_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pocet dat v cielovej tabulke*/
select count(*) as Init from Core.dbo.H_Customer

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#58d3f9dc604f4d27c3d51077b93e30aaf4294508'') IS NOT NULL) 
    DROP TABLE #58d3f9dc604f4d27c3d51077b93e30aaf4294508;

select * 
INTO #58d3f9dc604f4d27c3d51077b93e30aaf4294508
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        CID as Customer_ID
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

		INSERT INTO core.dbo.H_Customer
		(Customer_HK,LoadDatetime,SourceSystem,Customer_ID)
		SELECT tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.Customer_ID
		FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 AS tmp
		LEFT JOIN 	(SELECT b.Customer_HK, b.SourceSystem
							FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 b
							JOIN core.dbo.H_Customer
								on b.Customer_HK = core.dbo.H_Customer.Customer_HK
								and b.SourceSystem = core.dbo.H_Customer.SourceSystem
					) b
		ON tmp.Customer_HK = b.Customer_HK AND tmp.SourceSystem = b.SourceSystem
		WHERE b.Customer_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*zachovanie povodneho poctu zaznamov bez delete/truncate*/
select count(*) as Reload from Core.dbo.H_Customer

/*pridanie zaznamu na zdroji*/
INSERT INTO Stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (1000,'Test123','Garrison','30000101','Test123');

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#58d3f9dc604f4d27c3d51077b93e30aaf4294508'') IS NOT NULL) 
    DROP TABLE #58d3f9dc604f4d27c3d51077b93e30aaf4294508;

select * 
INTO #58d3f9dc604f4d27c3d51077b93e30aaf4294508
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        CID as Customer_ID
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

		INSERT INTO core.dbo.H_Customer
		(Customer_HK,LoadDatetime,SourceSystem,Customer_ID)
		SELECT tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.Customer_ID
		FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 AS tmp
		LEFT JOIN 	(SELECT b.Customer_HK, b.SourceSystem
							FROM #58d3f9dc604f4d27c3d51077b93e30aaf4294508 b
							JOIN core.dbo.H_Customer
								on b.Customer_HK = core.dbo.H_Customer.Customer_HK
								and b.SourceSystem = core.dbo.H_Customer.SourceSystem
					) b
		ON tmp.Customer_HK = b.Customer_HK AND tmp.SourceSystem = b.SourceSystem
		WHERE b.Customer_HK IS NULL;

	IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pridanie jedneho zaznamu*/
select count(*) as Inserted from Core.dbo.H_Customer

/*ukazka dat*/
select top 5 * from Core.dbo.H_Customer
order by loaddatetime desc

/*zmazanie pridanych testovacich dat*/
DELETE FROM Stage.dbo.Cust 
WHERE CID = 1000