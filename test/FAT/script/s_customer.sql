/*prazdna cielova tabulka*/
truncate table core.dbo.S_Customer

/*pocet dat na zdroji*/
select count(*) as Src
from (SELECT  *
	  FROM Stage.dbo.Cust) src

/*spustenie vygenerovanej procedury*/
exec(
'IF (OBJECT_ID(''tempdb..#eaf80e5d28107aa80139cd12803c610d9b8723fe'') IS NOT NULL) 
    DROP TABLE #eaf80e5d28107aa80139cd12803c610d9b8723fe;

select * 
INTO #eaf80e5d28107aa80139cd12803c610d9b8723fe
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
		1 as SourceSystem,
        HASHBYTES(''SHA1'', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,'' '',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.S_Customer
				(Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company)
				SELECT  tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.HashDiff, tmp.Name, tmp.BirthDate, tmp.Company
				FROM #eaf80e5d28107aa80139cd12803c610d9b8723fe AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by Customer_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.S_Customer as b )s
      						where rn = 1) AS b 
				ON tmp.Customer_HK = b.Customer_HK 
				AND tmp.SourceSystem = b.SourceSystem
   		WHERE b.Customer_HK IS NULL
   		OR tmp.HashDiff <> b.HashDiff;

IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pocet dat v cielovej tabulke*/
select count(*) as Init from Core.dbo.S_Customer 

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#eaf80e5d28107aa80139cd12803c610d9b8723fe'') IS NOT NULL) 
    DROP TABLE #eaf80e5d28107aa80139cd12803c610d9b8723fe;

select * 
INTO #eaf80e5d28107aa80139cd12803c610d9b8723fe
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
		1 as SourceSystem,
        HASHBYTES(''SHA1'', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,'' '',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.S_Customer
				(Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company)
				SELECT  tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.HashDiff, tmp.Name, tmp.BirthDate, tmp.Company
				FROM #eaf80e5d28107aa80139cd12803c610d9b8723fe AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by Customer_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.S_Customer as b )s
      						where rn = 1) AS b 
				ON tmp.Customer_HK = b.Customer_HK 
				AND tmp.SourceSystem = b.SourceSystem
   		WHERE b.Customer_HK IS NULL
   		OR tmp.HashDiff <> b.HashDiff;

IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*zachovanie povodneho poctu zaznamov bez delete/truncate*/
select count(*) as Reload from Core.dbo.S_Customer 

/*pridanie zaznamu na zdroji*/
INSERT INTO Stage.dbo.Cust (CID,FirstName,LastName,BirthDate,Company) VALUES (1000,'Test123','Garrison','30000101','Test123');

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#eaf80e5d28107aa80139cd12803c610d9b8723fe'') IS NOT NULL) 
    DROP TABLE #eaf80e5d28107aa80139cd12803c610d9b8723fe;

select * 
INTO #eaf80e5d28107aa80139cd12803c610d9b8723fe
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
		1 as SourceSystem,
        HASHBYTES(''SHA1'', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,'' '',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.S_Customer
				(Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company)
				SELECT  tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.HashDiff, tmp.Name, tmp.BirthDate, tmp.Company
				FROM #eaf80e5d28107aa80139cd12803c610d9b8723fe AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by Customer_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.S_Customer as b )s
      						where rn = 1) AS b 
				ON tmp.Customer_HK = b.Customer_HK 
				AND tmp.SourceSystem = b.SourceSystem
   		WHERE b.Customer_HK IS NULL
   		OR tmp.HashDiff <> b.HashDiff;

IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pridanie jedneho zaznamu*/
select count(*) as Inserted from Core.dbo.S_Customer 

/*update testovacieho zaznamu*/
update Stage.dbo.Cust
SET
LastName = 'TEST123'
WHERE CID = 1000

/*znovuspustenie*/
exec(
'IF (OBJECT_ID(''tempdb..#eaf80e5d28107aa80139cd12803c610d9b8723fe'') IS NOT NULL) 
    DROP TABLE #eaf80e5d28107aa80139cd12803c610d9b8723fe;

select * 
INTO #eaf80e5d28107aa80139cd12803c610d9b8723fe
from (SELECT  HASHBYTES(''SHA1'', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
		1 as SourceSystem,
        HASHBYTES(''SHA1'', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,'' '',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust) src

BEGIN TRY
	BEGIN TRANSACTION

    	INSERT INTO core.dbo.S_Customer
				(Customer_HK,LoadDatetime,SourceSystem,HashDiff,Name,BirthDate,Company)
				SELECT  tmp.Customer_HK, tmp.LoadDatetime, tmp.SourceSystem, tmp.HashDiff, tmp.Name, tmp.BirthDate, tmp.Company
				FROM #eaf80e5d28107aa80139cd12803c610d9b8723fe AS tmp
				LEFT JOIN (select *
       					   from
      					   (select b.*, row_number() over(partition by Customer_HK 
							  								order by b.LoadDatetime desc)rn 
							from core.dbo.S_Customer as b )s
      						where rn = 1) AS b 
				ON tmp.Customer_HK = b.Customer_HK 
				AND tmp.SourceSystem = b.SourceSystem
   		WHERE b.Customer_HK IS NULL
   		OR tmp.HashDiff <> b.HashDiff;

IF (@@TRANCOUNT > 0)
    	COMMIT
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
         ROLLBACK

	IF (1 = 1)
    BEGIN
		DECLARE @errorMessage NVARCHAR(MAX)
		SELECT @errorMessage = ERROR_MESSAGE()
    	RAISERROR (@errorMessage, 16, 1); 
    END
END CATCH')

/*pridanie zmeneneho zaznamu*/
select count(*) as Updated from Core.dbo.S_Customer 

/*ukazka dat*/
select top 5 * from Core.dbo.S_Customer
order by LoadDatetime desc

/*zmazanie pridanych testovacich dat*/
DELETE FROM Stage.dbo.Cust 
WHERE CID = 1000