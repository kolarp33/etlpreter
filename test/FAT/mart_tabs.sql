IF (OBJECT_ID('mart.dbo.Customer') IS NOT NULL) 
	DROP TABLE mart.dbo.Customer;

/*
DROP DATABASE mart
go
*/

CREATE DATABASE Mart
GO

CREATE TABLE mart.dbo.Customer(
    Customer_ID nvarchar(100),
    Name nvarchar(255),
    BirthDate date,
    Company nvarchar(255),
    ValidFrom datetime not null,
    ValidTo datetime not null,
    CurrentFlag bit not null,
)

