IF (OBJECT_ID('Core.dbo.H_Customer') IS NOT NULL) 
	DROP TABLE Core.dbo.H_Customer;

IF (OBJECT_ID('Core.dbo.S_Customer') IS NOT NULL) 
	DROP TABLE Core.dbo.S_Customer;

IF (OBJECT_ID('Core.dbo.L_CustomerAddress') IS NOT NULL) 
	DROP TABLE Core.dbo.L_CustomerAddress;

IF (OBJECT_ID('Core.dbo.H_Address') IS NOT NULL) 
	DROP TABLE Core.dbo.H_Address;

IF (OBJECT_ID('Core.dbo.S_Address') IS NOT NULL) 
	DROP TABLE Core.dbo.S_Address;

/*
DROP DATABASE Core
GO
*/

CREATE DATABASE Core
GO

CREATE TABLE Core.dbo.H_Customer(
    Customer_SK BIGINT NOT NULL IDENTITY (1,1),
    Customer_HK varbinary(40) NOT NULL,
    LoadDatetime datetime NOT NULL,
    SourceSystem int NOT NULL,
    Customer_ID nvarchar(100) NOT NULL,
    PRIMARY KEY (Customer_SK), 
    UNIQUE (Customer_HK)
)

CREATE TABLE Core.dbo.S_Customer(
    Customer_SK BIGINT NOT NULL IDENTITY (1,1),
    Customer_HK varbinary(40) NOT NULL,
    LoadDatetime datetime NOT NULL,
    SourceSystem int NOT NULL,
    HashDiff varbinary(40) NOT NULL,
    Name nvarchar(255),
    BirthDate date,
    Company nvarchar(255),

    PRIMARY KEY (Customer_SK) 
)

CREATE TABLE Core.dbo.L_CustomerAddress(
    CustomerAddress_SK BIGINT NOT NULL IDENTITY (1,1),
    CustomerAddress_HK varbinary(40) NOT NULL,
    Customer_HK varbinary(40) NOT NULL,
    Address_HK varbinary(40) NOT NULL,
    LoadDatetime datetime NOT NULL,
    SourceSystem int NOT NULL,

    PRIMARY KEY (CustomerAddress_SK),
    UNIQUE (CustomerAddress_HK)
)

CREATE TABLE Core.dbo.H_Address(
    Address_SK BIGINT NOT NULL IDENTITY (1,1),
    Address_HK varbinary(40) NOT NULL,
    LoadDatetime datetime NOT NULL,
    SourceSystem int NOT NULL,
    Address_ID nvarchar(100) NOT NULL,
    PRIMARY KEY (Address_SK),
    UNIQUE (Address_HK)
)

CREATE TABLE Core.dbo.S_Address(
    Address_SK BIGINT NOT NULL IDENTITY (1,1),
    Address_HK varbinary(40) NOT NULL,
    LoadDatetime datetime NOT NULL,
    HashDiff varbinary(40) NOT NULL,
    Street varchar(255),
    City varchar(255),
    Country varchar(100),
    ZipCode varchar(10),
    
    PRIMARY KEY (Address_SK) 
)