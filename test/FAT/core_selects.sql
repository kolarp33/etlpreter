/*HUB CUSTOMER*/
SELECT  HASHBYTES(\'\'SHA1\'\', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        CID as Customer_ID
FROM stage.dbo.Cust

/*SAT CUSTOMER*/
SELECT  HASHBYTES(\'\'SHA1\'\', CAST(CID as nvarchar)) as Customer_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        HASHBYTES(\'\'SHA1\'\', CONCAT(FirstName,LastName,CAST(BirthDate as nvarchar),Company)) as HashDiff,
        CONCAT(FirstName,\'\' \'\',LastName) as Name,
        BirthDate,
        Company
FROM stage.dbo.Cust

/*HUB Addr*/
/*
SELECT  HASHBYTES('SHA1', CAST(AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem,
        AID as Address_ID
FROM stage.Addr
*/
/*SAT Addr*/
/*
SELECT  HASHBYTES('SHA1', CAST(AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        HASHBYTES('SHA1', CONCAT(Street,City,Country,Zip)) as HashDiff,
        Street,
        City,
        Country,
        Zip as ZipCode
FROM stage.Addr
*/

/*LNK CustAddr*/
SELECT  HASHBYTES(\'\'SHA1\'\', CONCAT(CAST(Cust.CID as nvarchar),CAST(adr.AID as nvarchar))) as CustomerAddress_HK,
        HASHBYTES(\'\'SHA1\'\', CAST(Cust.CID as nvarchar)) as Customer_HK,
        HASHBYTES(\'\'SHA1\'\', CAST(adr.AID as nvarchar)) as Address_HK,
        GETDATE() as LoadDatetime,
        1 as SourceSystem
FROM stage.dbo.Addr adr
JOIN stage.dbo.Cust cust ON adr.CID = cust.CID