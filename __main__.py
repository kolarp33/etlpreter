import sys
from model.tables import *
from model.database import *
from core.etlpreter import ETLPreter
from exc.BadApiCallError import *
from exc.MissingValueError import *


class ETL_Tools():

    def __init__(self, **kwargs):
        return super().__init__(**kwargs)

    # def

    def main(self):

        connection = Database()
        session = connection.connect()

        etlpreter = ETLPreter()

        # etlpreter bude dostavat vybrane mapovania ktore ma generovat po riadku
        maps = session.query(etl_map).all()

        try:
            for map in maps:
                interpreted = etlpreter.interpret(map)
                session.bulk_save_objects(interpreted)
                session.commit()
        except BadApiCallError as api_e:
            print('Error interpreting: {0}, {1}'.format(
                api_e.call, api_e.message))
        except MissingValueError as value_e:
            print('Missing value: {0}, {1}'.format(
                value_e.attribute, value_e.message))
        finally:
            session.close()
            # for i in interpreted:
            #     print(i.etl_map_id, i.etl_km_id, i.etl_km_steps_id,
            #           i.etl_km_step, i.loaddatetime)

        # steps = session.query(etl_km_steps).all()
        # for step in steps:
        #     print(step.km.name)

        # kms = session.query(etl_knowledge_module).all()
        # for km in kms:
        #     for step in km.ETL_KM_STEPS:
        #         print(step.step)


if __name__ == "__main__":
    app = ETL_Tools()
    app.main()
