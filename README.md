# ETLPreter
**ETLPreter** serves as a solution for **dynamically generated runnable SQL** code transferring the data. The generating is based on metadata stored in a relational database and knowledge modules, which contain **API calls** for dynamical insert of the data. This extension is free to use, and it may serve to whomever as a **base for their ETL tool**.

## How to
This is an example, how to work with this extension
1. Create metadata table scructure from the create script
2. Insert desired metadata for dynamicall generation
3. Run main
4. Data has been added to ETL_HIST table
5. Extract those interpreted data by SQL 
6. Run as dynamic command in your database